import lastfm, os, sys, shutil, csv, getopt, datetime, time
from urllib import urlretrieve
from mutagen.mp3 import MP3
from mutagen.id3 import ID3, APIC, error
from mutagen.easyid3 import EasyID3

'''utf-8 filename encode'''
#def encode(path):

'''runner'''
def main(argv):
	currentpath = {}

	# parse cli args
	try:
		opts, args = getopt.getopt(argv, 'hd:', ['ddir='])
		for opt, arg in opts:
		    if opt == "-h":
		    	print("albumdisc.py -d <dir>")
		    	sys.exit()
		    elif opt in ("-d", "--ddir"):
		      	currentpath = arg
	except getopt.GetoptError:
		print('albumdisc.py -d <dir>')
		sys.exit()

	#check input file
	if not currentpath:
		print('albumdisc.py -d <dir>')
		sys.exit()

	# valid input file?
	if not os.path.exists(currentpath):
		print('No directory ' + currentpath)
		sys.exit()

	# iterate albums pulling directories
	# with multiple discs
	EasyID3.RegisterTextKey("discnumber", "TPOS")
	for album in os.listdir(currentpath):
		albumpath = os.path.join(currentpath, album)
		try:
			for song in os.listdir(albumpath):
				if not song.lower().endswith('.mp3'):
					continue
				songpath = os.path.join(currentpath, album, song)
				try:
					id3 = EasyID3(songpath)
				except:
					continue
				if not id3['discnumber'][0][:2] == '01':
					print(albumpath)
					break
		except:
			print('encoding error?: ' + albumpath)

if __name__ == '__main__':
	main(sys.argv[1:])