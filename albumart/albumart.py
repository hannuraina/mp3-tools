import lastfm, os, sys, shutil, csv, getopt, datetime, time
from urllib import urlretrieve
from mutagen.mp3 import MP3
from mutagen.id3 import ID3, APIC, error
from mutagen.easyid3 import EasyID3

#audioscrobbler api key
api_key = "a5d060187f3a863f67770a9872bd9da0"
#api wrapper
api = lastfm.Api(api_key)
#temp store for images
tmp_dir = 'tmp'
#strings to omit for audioscrobbler query
replacements = [' original soundtrack', ' OST' ]
# log
log = {}
log_file = 'albumart.log'

'''save album art as front cover in id3v2
for all songs in specified path'''
def write_tag(image, album):
	for song in os.listdir(album):
		if (song.lower().endswith('.mp3')):
			track = MP3(os.path.join(album, song), ID3=ID3)
			try:
				track.add_tags()
			except error:
				pass
			format = image[-3:]
			track.tags.delall('APIC') #clear out existing images
			track.tags.add(APIC(encoding=3, mime='image/'+format, type=3 ,desc=u'',data=open(image, 'rb').read()))
			track.save()

'''query audioscrobbler for artist and album'''
def get_image(artist, album):
	# try removing original soundtrack
	for r in replacements:
		album = album.lower().replace(r, '')

	try:
		obj = api.get_album(album, artist)
	except:
		return

	if obj.image['extralarge']:
		url = obj.image['extralarge']
	elif obj.image['large']:
		url = obj.image['large']
	elif obj.image['medium']:
		url = obj.image['medium']
	elif obj.image['small']:
		url = obj.image['small']
	else:
		return

	try:
		filename = os.path.join(tmp_dir,artist+"-"+album+url[-4:])
		urlretrieve(url, filename)
		return filename
	except:
		return
		# log('FAIL - Unable to retrieve image from ' + url)

'''pull album information from id3 tags'''
def get_info(album):
	EasyID3.RegisterTextKey("albumartist", "TPE2")
	for song in os.listdir(album):
		if (song.lower().endswith('.mp3')):
			id3 = EasyID3(os.path.join(album,song))
			return { 'albumartist': id3['albumartist'][0], 'artist': id3['artist'][0], 'album': id3['album'][0] }

'''driver for tagging process
queries for albumartist then artist
if no results found'''
def tag(path):
	# get album id3s
	try:
		id3 = get_info(path)
		if not id3['albumartist'] and not id3['artist']:
			raise
	except:
		log('FAIL - no id3 for ' + path + ' (possible filetype error?)')
		return


	# check albumartist - album, followed by songartist - album
	image = get_image(id3['albumartist'], id3['album'])
	if not image:
		image = get_image(id3['artist'], id3['album'])
		if not image:
			log('FAIL - No images found for ' + path)
			return

	try:
		write_tag(image, path)
		# log("SUCCESS - added art for " + path)
	except:
		log('FAIL - Error saving tag for ' + path)

'''logging'''
def log(text):
	log = open(log_file, 'ab')
	log.write('[' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ']: ' + text + '\r\n')

'''clean up temporary directory and images'''
def cleanup():
	try:
		shutil.rmtree(tmp_dir)
	except:
		log('Error cleaning up temp directory')

'''setup temp directories'''
def setup():
	try :
		os.makedirs(tmp_dir)
	except OSError:
		pass
	# initialize new log file
	log = open(log_file, 'wb')

'''runner'''
def main(argv):
	current_path, inputfile = {}, {}

	# parse cli args
	try:
		opts, args = getopt.getopt(argv, 'hf:', ['ffile='])
		for opt, arg in opts:
		    if opt == "-h":
		    	print 'albumart.py -f <filepath.csv>'
		    	sys.exit()
		    elif opt in ("-f", "--ffile") and arg.lower().endswith('.csv'):
		      	inputfile = arg
	except getopt.GetoptError:
		print 'albumart.py -f <filepath>'
		sys.exit()

	#check input file
	if not inputfile:
		print 'albumart.py -f <filepath.csv>'
		sys.exit()

	# valid input file?
	if not os.path.isfile(inputfile):
		print('Error importing filelist ' + inputfile)
		sys.exit()

	# setup env
	setup()

	# iterate song list from input file
	with open(inputfile, 'rb') as csvfile:
		csvreader = csv.DictReader(csvfile)
		for row in csvreader:
			if row['Path'] == '':
				continue
			path = row['Path'].split('\\')
			path.pop()
			path = '\\'.join(path)

			# one query per folder
			if not current_path == path:
				current_path = path
				tag(current_path)

    # remove tmp data
	cleanup()

if __name__ == '__main__':
	main(sys.argv[1:])