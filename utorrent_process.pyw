# utorrent_process.py
import sys, os, getopt
from subprocess import call

tvrename = "D:\TVRename\TVRename.exe"
music_path = "G:\new"

def usage():
  print ('utorrent_process.py --label <label> --path <path>')

def main(argv):
  label, path, filepath = None, None, None

  try:
    opts, args = getopt.getopt(argv, 'lpf:', ['label=','path=','filepath='])
  except getopt.GetoptError as err:
    print (err)
    usage()
    sys.exit(2)

  for opt, arg in opts:
    if opt in ('l', '--label'):
      label = arg
    elif opt in ('p', '--path'):
      path = arg
    elif opt in ('f', '--filepath'):
      filepath = arg

  
  if not label or not path:
    usage()
    sys.exit(2)

  if label == 'tv':
    os.system(" ".join([tvrename, '/doall /hide /quit']))
  elif label == 'music':
    base, target = os.path.split(path)
    new_path = "\\".join([music_path, target])
    if filepath:
      call (" ".join(['cp', '"' + path + '"', '"' + new_path + '"']))
    else:
      call (" ".join(['xcopy', '/ysie', '"' + path + '"', '"' + new_path + '"']))
  else:
    sys.exit(2)

if __name__ == '__main__':
  main(sys.argv[1:])